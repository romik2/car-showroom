CREATE TABLE clients (
    code INT PRIMARY KEY,
    full_name VARCHAR(50),
    phone VARCHAR(20),
    email VARCHAR(50)
);

CREATE TABLE payments (
    id SERIAL PRIMARY KEY,
    amount DECIMAL(10, 2),
    payment_date DATE,
    payment_type VARCHAR(50)
);

CREATE TABLE contracts (
    code INT PRIMARY KEY,
    date DATE,
    employee_id INT,
    car_id INT,
    client_id INT,
    payment_id INT,
    status VARCHAR(20),
    passport_data TEXT
);

CREATE TABLE car_brand (
    code INT PRIMARY KEY,
    brand VARCHAR(50),
    model VARCHAR(50),
    color VARCHAR(20),
    length DECIMAL(5, 2),
    width DECIMAL(5, 2),
    height DECIMAL(5, 2),
    seats INT,
    max_speed INT,
    engine_capacity INT,
    fuel_consumption DECIMAL(5, 2)
);

CREATE TABLE employees (
    code INT PRIMARY KEY,
    full_name VARCHAR(50),
    position_id INT,
    birth_date DATE,
    hire_date DATE,
    salary DECIMAL(10, 2),
    phone VARCHAR(20),
    email VARCHAR(50)
);

CREATE TABLE cars (
    code INT PRIMARY KEY,
    brand_id INT,
    supplier_id INT,
    price DECIMAL(10, 2),
    availability BOOLEAN,
    model VARCHAR(50)
);

CREATE TABLE positions (
    code INT PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE suppliers (
    code INT PRIMARY KEY,
    name VARCHAR(50),
    contact_info TEXT,
    note TEXT
);

CREATE TABLE test_drive (
    code INT PRIMARY KEY,
    date DATE,
    time TIME,
    car_id INT,
    note TEXT
);

ALTER TABLE contracts
ADD CONSTRAINT fk_employee_id
FOREIGN KEY (employee_id) REFERENCES employees (code);

ALTER TABLE contracts
ADD CONSTRAINT fk_car_id
FOREIGN KEY (car_id) REFERENCES cars (code);

ALTER TABLE contracts
ADD CONSTRAINT fk_client_id
FOREIGN KEY (client_id) REFERENCES clients (code);

ALTER TABLE contracts
ADD CONSTRAINT fk_payment_id
FOREIGN KEY (payment_id) REFERENCES payments (id);

ALTER TABLE employees
ADD CONSTRAINT fk_position_id
FOREIGN KEY (position_id) REFERENCES positions (code);

ALTER TABLE cars
ADD CONSTRAINT fk_brand_id
FOREIGN KEY (brand_id) REFERENCES car_brand (code);

ALTER TABLE cars
ADD CONSTRAINT fk_supplier_id
FOREIGN KEY (supplier_id) REFERENCES suppliers (code);

ALTER TABLE test_drive
ADD CONSTRAINT fk_car_id
FOREIGN KEY (car_id) REFERENCES cars (code);

CREATE SEQUENCE clients_code_seq START 1;
ALTER SEQUENCE clients_code_seq OWNED BY clients.code;
ALTER TABLE clients ALTER COLUMN code SET DEFAULT nextval('clients_code_seq');

CREATE SEQUENCE contracts_code_seq START 1;
ALTER SEQUENCE contracts_code_seq OWNED BY contracts.code;
ALTER TABLE contracts ALTER COLUMN code SET DEFAULT nextval('contracts_code_seq');

CREATE SEQUENCE car_brand_code_seq START 1;
ALTER SEQUENCE car_brand_code_seq OWNED BY car_brand.code;
ALTER TABLE car_brand ALTER COLUMN code SET DEFAULT nextval('car_brand_code_seq');

CREATE SEQUENCE employees_code_seq START 1;
ALTER SEQUENCE employees_code_seq OWNED BY employees.code;
ALTER TABLE employees ALTER COLUMN code SET DEFAULT nextval('employees_code_seq');

CREATE SEQUENCE cars_code_seq START 1;
ALTER SEQUENCE cars_code_seq OWNED BY cars.code;
ALTER TABLE cars ALTER COLUMN code SET DEFAULT nextval('cars_code_seq');

CREATE SEQUENCE positions_code_seq START 1;
ALTER SEQUENCE positions_code_seq OWNED BY positions.code;
ALTER TABLE positions ALTER COLUMN code SET DEFAULT nextval('positions_code_seq');

CREATE SEQUENCE suppliers_code_seq START 1;
ALTER SEQUENCE suppliers_code_seq OWNED BY suppliers.code;
ALTER TABLE suppliers ALTER COLUMN code SET DEFAULT nextval('suppliers_code_seq');

CREATE SEQUENCE test_drive_code_seq START 1;
ALTER SEQUENCE test_drive_code_seq OWNED BY test_drive.code;
ALTER TABLE test_drive ALTER COLUMN code SET DEFAULT nextval('test_drive_code_seq');